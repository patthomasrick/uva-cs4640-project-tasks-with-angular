# CS 4640 Project - Tasks

> Patrick Thomas `pwt5ca`

> Now with Angular

## API Documentation

### `/login.php` - `POST` or `GET`

#### `POST`

If the method is `POST`, then the API attempts to create a session and login with the supplied information as shown below:

```json
{
  "username": "my_username",
  "password": "my_password"
}
```

Response:

```json
{
  "success": true
}
```

#### `GET`

If the method is `GET`, then the API simply tries to verify that there is a user currently logged in with the current session.

Response:

```json
{
  "success": true
}
```

### `/verify.php` - `POST`

Given a username and session ID, verify that the session is valid.

```json
{
  "username": "my_username",
  "session_id": "954df792b4fd7a3a388694a4e353617985b79ca45238af2efbbdc708f6f14be18ff5665b10738c4d7d6a7cced046632024f051becfb641e04d955bbe38c7dd04"
}
```

Response:

```json
{
  "success": true
}
```
