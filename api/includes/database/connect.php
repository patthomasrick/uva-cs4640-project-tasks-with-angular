<?php

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  http_response_code(404);
  die();
}

require_once "secret.php";

try {
  global $dbh;
  $dbh = new PDO($DSN, $USER, $PASSWORD, [
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = 'Etc/UTC'",
  ]);
} catch (PDOException $e) {
  print "Database error: " . $e->getMessage() . "<br/>";
  die();
}
