<?php

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  http_response_code(404);
  die();
}

function headers_json_cors()
{
  header("Access-Control-Allow-Origin: *");
  header(
    "Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding"
  );
  header("Access-Control-Max-Age: 1000");
  header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
  header("Content-Type: application/json");
}
