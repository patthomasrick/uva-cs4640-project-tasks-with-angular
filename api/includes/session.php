<?php

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  http_response_code(404);
  die();
}

// Start the session once and only once whenever needed.
session_start();
