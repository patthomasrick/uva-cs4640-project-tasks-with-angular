<?php

require_once "database/connect.php";
require_once "session.php";
require_once "utils.php";

global $login_failed;
$login_failed = null;

global $creation_failed;
$creation_failed = null;

if (array_key_exists("nav-log-out", $_GET)) {
  session_destroy();
  redirect_if_not_logged_in();
  die();
}

if (
  array_key_exists("loginForm", $_POST) &&
  array_key_exists("username", $_POST) &&
  array_key_exists("password", $_POST)
) {
  // Handle login forms across the application.
  $username = $_POST["username"];
  $password = $_POST["password"];

  global $login_failed;
  $login_failed = true;

  if (strlen($username) == 0 || strlen($password) == 0) {
    // Do nothing if server side validation fails.
  } else {
    global $login_failed;
    $login_failed = is_null(user_login($username, $password));
    if (!$login_failed) {
      header("Location: /", 300);
    }
  }
} elseif (
  array_key_exists("creationForm", $_POST) &&
  array_key_exists("username", $_POST) &&
  array_key_exists("newPassword", $_POST) &&
  array_key_exists("newPassword2", $_POST)
) {
  // Handle account creation forms across the application.
  $username = $_POST["username"];
  $newPassword = $_POST["newPassword"];
  $newPassword2 = $_POST["newPassword2"];

  global $creation_failed;
  $creation_failed = true;

  if (
    strlen($username) == 0 ||
    strlen($newPassword) == 0 ||
    strlen($newPassword2) == 0 ||
    $newPassword !== $newPassword2
  ) {
    // Do nothing if server side validation fails.
  } else {
    $creation_failed = !user_create($username, $newPassword);
    if (!$creation_failed) {
      header("Location: login.php", 300);
    }
  }
}

class User
{
  public $username = "";
  public $session_id = "";
  public $user_meta = [];
  public $tasks = [];

  public function login($username, $password, $session_id = null)
  {
    /**
     * Log a user in, setting some session variables.
     */
    global $dbh;
    $sql =
      "SELECT `password`, `session_id` FROM `users` WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([":username" => $username]);

    $result = $cursor->fetch(PDO::FETCH_ASSOC);
    $stored_password = $result["password"];
    $stored_session_id = $result["password"];

    $cursor->closeCursor();

    if ($success === false) {
      return false;
    } elseif ($session_id && password_verify($session_id, $stored_session_id)) {
      $this->username = $username;
      // $this->get_session_id();
      $this->update_last_login();
      $this->refresh_tasks();
      $_SESSION["current_user"] = $this;
      return true;
    } elseif (password_verify($password, $stored_password)) {
      $this->username = $username;
      if (!$this->get_session_id()) {
        die("Could not set session ID.");
      }
      $this->update_last_login();
      $this->refresh_tasks();
      $_SESSION["current_user"] = $this;
      return true;
    } else {
      return false;
    }
  }

  public function create($username, $password)
  {
    /**
     * Create a user, registering it with the database.
     */
    global $dbh;
    $sql =
      "INSERT INTO `users` (`username`, `password`, `user_meta`) VALUES (:username, :password, '{}');";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $username,
      ":password" => password_hash($password, PASSWORD_BCRYPT),
    ]);
    $cursor->closeCursor();

    return (bool) $success;
  }

  public function log_out()
  {
    /**
     * Destroy the current session and erase variables in the current user.
     */
    $this->username = "";
    $this->session_id = "";
    $this->user_meta = [];
    $this->tasks = [];
    $_SESSION["current_user"] = null;
    session_destroy();
  }

  private function get_session_id()
  {
    /**
     * Register a user with the database and set up a session, which in this case is like a
     * temporary API key between the server and machine.
     */
    global $dbh;
    $session_id = bin2hex(random_bytes(64));
    $session_id_hashed = password_hash($session_id, PASSWORD_BCRYPT);
    $sql =
      "UPDATE `users` SET `session_id` = :session_id WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $this->username,
      ":session_id" => $session_id_hashed,
    ]);
    $cursor->closeCursor();
    if ($success) {
      $this->session_id = $session_id;
    }
    return (bool) $success;
  }

  public function change_password($new_password)
  {
    /**
     * Register a user with the database and set up a session, which in this case is like a
     * temporary API key between the server and machine.
     */
    global $dbh;
    $new_password_hashed = password_hash($new_password, PASSWORD_BCRYPT);
    $sql =
      "UPDATE `users` SET `password` = :password WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $this->username,
      ":password" => $new_password_hashed,
    ]);
    $cursor->closeCursor();

    return (bool) $success;
  }

  public function verify_session()
  {
    /**
     * Make sure that a session ID and username is uniform. This ensures that a user can only be
     * logged in on one machine at a time.
     */
    global $dbh;
    $sql = "SELECT `session_id` FROM `users` WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([":username" => $this->username]);
    $result = $cursor->fetch(PDO::FETCH_ASSOC);
    $stored_session_id = $result["session_id"];
    $cursor->closeCursor();

    if ($success === false) {
      return false;
    } else {
      return password_verify($this->session_id, $stored_session_id);
    }
  }

  private function update_last_login()
  {
    /**
     * Set on the server when this user last logged in.
     */
    global $dbh;
    $sql =
      "UPDATE `users` SET `last_login_date` = CURRENT_TIMESTAMP WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([":username" => $this->username]);
    $cursor->closeCursor();
    return (bool) $success;
  }

  private function refresh_user_meta()
  {
    /**
     * Download fresh user_meta data from the server.
     */
    global $dbh;
    $sql = "SELECT `user_meta` FROM `users` WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([":username" => $this->username]);
    $result = $cursor->fetch(PDO::FETCH_ASSOC);
    $this->user_meta = json_decode($result["user_meta"], JSON_OBJECT_AS_ARRAY);
    $cursor->closeCursor();
    return (bool) $success;
  }

  private function refresh_tasks()
  {
    /**
     * Download fresh user_meta data from the server.
     */
    global $dbh;
    $sql = "SELECT * FROM `tasks` WHERE `username`=:username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([":username" => $this->username]);
    $results = $cursor->fetchAll(PDO::FETCH_ASSOC);
    $cursor->closeCursor();
    $this->tasks = $results;
    return (bool) $success;
  }

  public function get_all_user_meta()
  {
    /**
     * Get an array of all user meta data.
     */
    $this->refresh_user_meta();
    return $this->user_meta;
  }

  private function upload_user_meta($new_meta)
  {
    /**
     * Upload the current metadata array.
     */
    global $dbh;
    $sql =
      "UPDATE `users` SET `user_meta` = :new_meta WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $this->username,
      ":new_meta" => json_encode($new_meta),
    ]);
    $cursor->closeCursor();

    $this->refresh_user_meta();

    return (bool) $success;
  }

  public function add_user_meta($key, $value)
  {
    /**
     * Add a user meta key and value to the array, and upload that data. The key must not already
     * exist in the array.
     */
    if (array_key_exists($key, $this->user_meta)) {
      return false;
    }
    $new_meta = $this->user_meta;
    $new_meta["$key"] = $value;
    return $this->upload_user_meta($new_meta);
  }

  public function update_user_meta($key, $value)
  {
    /**
     * Change some existing user meta data. The key may or may not already exist in the database.
     */
    $new_meta = $this->user_meta;
    $new_meta["$key"] = $value;
    return $this->upload_user_meta($new_meta);
  }

  public function delete_user_meta($key)
  {
    /**
     * Remove some user meta key.
     */
    $new_meta = $this->user_meta;
    unset($new_meta["$key"]);
    return $this->upload_user_meta($new_meta);
  }

  public function get_all_tasks()
  {
    /**
     * Get an array of all user meta data.
     */
    // $this->refresh_tasks();
    return $this->tasks;
  }

  public function new_task(
    $title,
    $description = null,
    $date_scheduled = null,
    $date_due = null,
    $category = null
  ) {
    $task_data = [
      ":title" => $title,
      ":username" => $this->username,
      ":description" => $description,
      ":date_scheduled" => $date_scheduled,
      ":date_due" => $date_due,
      ":category" => $category,
    ];

    // 4/11/2021, 4:30:24 PM
    // '%m/%d/%Y, %l:%i:%s %p'

    global $dbh;
    $sql = "INSERT INTO `tasks` (`username`, `title`, `description`, `category`, `date_due`, `date_scheduled`) 
      VALUES (:username, :title, :description, :category, STR_TO_DATE(:date_due, '%m/%d/%Y, %l:%i:%s %p'), STR_TO_DATE(:date_scheduled, '%m/%d/%Y, %l:%i:%s %p'));";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute($task_data);
    $cursor->closeCursor();
    $this->refresh_tasks();
    return (bool) $success;
  }

  public function update_task(
    $title,
    $id,
    $description = null,
    $date_scheduled = null,
    $date_due = null,
    $category = null,
    $is_done = false,
    $is_deleted = false
  ) {
    $task_data = [
      ":username" => $this->username,
      ":id" => $id,
      ":title" => $title,
      ":description" => $description,
      ":date_scheduled" => $date_scheduled,
      ":date_due" => $date_due,
      ":category" => $category,
      ":is_done" => $is_done,
      ":is_deleted" => $is_deleted,
    ];

    // 4/11/2021, 4:30:24 PM
    // '%m/%d/%Y, %l:%i:%s %p'

    global $dbh;
    $sql = "UPDATE `tasks`
    SET `title` = :title,
    `description` = :description,
    `date_scheduled` = STR_TO_DATE(:date_scheduled, '%m/%d/%Y, %l:%i:%s %p'),
    `date_due` = STR_TO_DATE(:date_due, '%m/%d/%Y, %l:%i:%s %p'),
    `category` = :category,
    `is_done` = :is_done,
    `is_deleted` = :is_deleted
    WHERE `tasks`.`id` = :id AND `tasks`.`username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute($task_data);
    $cursor->closeCursor();
    $this->refresh_tasks();
    return (bool) $success;
  }

  public function delete_task($task_id)
  {
    // $task_data[":username"] = $this->username;

    global $dbh;
    $sql = "DELETE FROM `tasks` WHERE `id` = :id AND `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $this->username,
      ":id" => $task_id,
    ]);
    $cursor->closeCursor();
    $this->refresh_tasks();
    return (bool) $success;
  }

  public function delete_user()
  {
    global $dbh;
    $sql = "DELETE FROM `tasks` WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $cursor->execute([
      ":username" => $this->username,
    ]);

    $sql = "DELETE FROM `users` WHERE `username` = :username;";
    $cursor = $dbh->prepare($sql);
    $success = $cursor->execute([
      ":username" => $this->username,
    ]);
    $cursor->closeCursor();

    $this->log_out();

    return (bool) $success;
  }
}

function user_login($username, $password)
{
  /**
   * Helper function to easily log in and get a user object.
   */
  $user = new User();
  if ($user->login($username, $password)) {
    return $user;
  } else {
    return null;
  }
}

function user_session_login($username, $session_id)
{
  /**
   * Helper function to easily log in and get a user object.
   */
  $user = new User();
  if ($user->login($username, "", $session_id)) {
    return $user;
  } else {
    return null;
  }
}

function user_create($username, $password)
{
  /**
   * Helper function to easily create a user and get a user object.
   */
  $user = new User();
  if ($user->create($username, $password)) {
    return $user;
  } else {
    return null;
  }
}

function current_user()
{
  /**
   * Get the currently logged in user. If there is a current request to log out, then return null.
   * If the current user is not logged in or cannot be verified, then also return null. Otherwise
   * return the user.
   */
  if (
    session_status() === PHP_SESSION_ACTIVE and
    array_key_exists("current_user", $_SESSION)
  ) {
    $user = $_SESSION["current_user"];
    if (array_key_exists("nav-log-out", $_POST)) {
      $user->log_out();
      return null;
    } elseif ($user->verify_session()) {
      return $user;
    }
  }
  return null;
}
