<?php

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) {
  http_response_code(404);
  die();
}

function redirect_if_not_logged_in()
{
  $user = current_user();
  if (is_null($user)) {
    header("Location: /landing.php", 301);
  }
}

function redirect_if_logged_in()
{
  $user = current_user();
  if (!is_null($user)) {
    header("Location: /", 301);
  }
}
