<?php

if ($_SERVER["REQUEST_URI"] == "" || $_SERVER["REQUEST_URI"] == "/") {
  require __DIR__ . "/index.php";
} else {
  require __DIR__ . $_SERVER["REQUEST_URI"];
}
die();
