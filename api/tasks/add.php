<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/includes/user.php";
require_once "$root/includes/headers.php";
require_once "$root/includes/session.php";

headers_json_cors();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $post_data = file_get_contents("php://input");
  $request = json_decode($post_data, $flags = JSON_OBJECT_AS_ARRAY);
  $username = $request["username"];
  $password = $request["password"];

  $title = $request["title"];
  $description = $request["description"];
  $date_scheduled = $request["date_scheduled"];
  $date_due = $request["date_due"];
  $category = $request["category"];

  if (!$date_due) {
    $date_due = null;
  }
  if (!$date_scheduled) {
    $date_scheduled = null;
  }

  $user = user_login($username, $password);

  if (is_null($user)) {
    echo json_encode([
      "success" => false,
      "reason" => "Invalid credentials.",
    ]);
    die();
  }

  $success = $user->new_task(
    $title,
    $description,
    $date_scheduled,
    $date_due,
    $category
  );

  echo json_encode([
    "success" => $success,
    "user" => $user,
  ]);
  die();
}
