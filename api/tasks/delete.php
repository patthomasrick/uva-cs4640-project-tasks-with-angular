<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/includes/user.php";
require_once "$root/includes/headers.php";
require_once "$root/includes/session.php";

headers_json_cors();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $post_data = file_get_contents("php://input");
  $request = json_decode($post_data, $flags = JSON_OBJECT_AS_ARRAY);
  $username = $request["username"];
  $password = $request["password"];

  $id = $request["id"];

  $user = user_login($username, $password);

  if (is_null($user)) {
    echo json_encode([
      "success" => false,
      "reason" => "Invalid credentials.",
    ]);
    die();
  }

  $success = $user->delete_task($id);

  echo json_encode([
    "success" => $success,
    "user" => $user,
  ]);
  die();
}
