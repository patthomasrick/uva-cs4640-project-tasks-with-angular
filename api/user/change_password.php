<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/includes/user.php";
require_once "$root/includes/headers.php";
require_once "$root/includes/session.php";

headers_json_cors();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $post_data = file_get_contents("php://input");
  $request = json_decode($post_data, $flags = JSON_OBJECT_AS_ARRAY);
  $username = $request["username"];
  $password = $request["password"];
  $new_password = $request["new_password"];

  $user = user_login($username, $password);
  $user->change_password($new_password);
  $user = user_login($username, $new_password);

  echo json_encode([
    "success" => !is_null($user),
    "username" => $username,
  ]);
}
die();
