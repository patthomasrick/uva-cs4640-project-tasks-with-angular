<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once "$root/includes/user.php";
require_once "$root/includes/headers.php";
require_once "$root/includes/session.php";

headers_json_cors();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $post_data = file_get_contents("php://input");
  $request = json_decode($post_data, $flags = JSON_OBJECT_AS_ARRAY);
  $username = $request["username"];
  $session_id = $request["session_id"];

  $user = user_session_login($username, $session_id);

  echo json_encode([
    "success" => !is_null($user),
    "user" => $user,
    "request" => $request,
  ]);
}
die();
