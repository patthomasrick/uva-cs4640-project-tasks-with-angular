import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessagesService } from '../messages.service';
import { User, EMPTY_USER } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent {
  formOldPassword: string = '';
  formPassword: string = '';
  formPasswordVerify: string = '';
  formSubmissionSuccessful: boolean = false;
  deletePresses: number = 0;

  user: User = EMPTY_USER;

  constructor(
    private userService: UserService,
    private messageService: MessagesService,
    private router: Router
  ) {
    this.userService.getUser().subscribe((user) => {
      this.user = user;
      if (this.user.username == EMPTY_USER.username) {
        this.router.navigate(['']);
      }
    });
    document.title = 'Tasks | Account';
  }

  passwordVerifyValid(): boolean {
    return (
      this.formPassword == this.formPasswordVerify &&
      this.formPassword.length > 0 &&
      this.formPasswordVerify.length > 0
    );
  }

  passwordVerifyInvalid(): boolean {
    return (
      this.formPassword != this.formPasswordVerify &&
      this.formPassword.length > 0 &&
      this.formPasswordVerify.length > 0
    );
  }

  changePasswordDisable(): boolean {
    return (
      this.formSubmissionSuccessful ||
      this.passwordVerifyInvalid() ||
      this.formPassword == '' ||
      this.formPasswordVerify == ''
    );
  }

  onPasswordChange() {
    this.userService
      .changeUserPassword(this.formPassword)
      .subscribe((success) => {
        this.formSubmissionSuccessful = success;
        if (success) {
          this.messageService.sendMessage(
            'Password changed successfully!',
            'alert-success'
          );
        } else {
          this.messageService.sendMessage(
            'Request to change password was unsuccessful. Please try again later.',
            'alert-warning'
          );
        }
      });
  }

  onExport() {
    // From https://stackoverflow.com/a/62881364
    let exportedData = JSON.stringify(this.user, null, 1);
    let blob = new Blob([exportedData], { type: 'text/json' });
    let url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  onDeleteAccount() {
    this.deletePresses++;
    if (this.deletePresses > 1) {
      this.userService.deleteUser().subscribe((success) => {
        if (success) {
          this.messageService.sendMessage(
            'User deleted from database.',
            'alert-success'
          );
          // this.router.navigate(['']);
        } else {
          this.messageService.sendMessage(
            'Failed to delete user from database; contact Patrick.',
            'alert-danger'
          );
        }
      });
    }
  }
}
