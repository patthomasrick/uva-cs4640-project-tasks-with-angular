import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessagesService } from '../messages.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  formUsername: string = '';
  formPassword: string = '';
  formPasswordVerify: string = '';

  formSubmitted: boolean = false;
  formSubmissionSuccessful: boolean = false;

  constructor(
    private userService: UserService,
    private messageService: MessagesService,
    private router: Router
  ) {
    document.title = 'Tasks | Create';
  }

  ngOnInit(): void {}

  onSubmit() {
    // Attempt to create a user through the user service.
    if (this.usernameInvalid()) {
      this.messageService.sendMessage(
        'Invalid username; username must be one or more characters long.',
        'alert-danger'
      );
      return;
    }
    if (this.passwordInvalid()) {
      this.messageService.sendMessage(
        'Invalid password; password must be eight or more characters long.',
        'alert-danger'
      );
      return;
    } else if (this.passwordVerifyInvalid()) {
      this.messageService.sendMessage(
        'The entered passwords do not match.',
        'alert-danger'
      );
      return;
    }
    this.userService
      .createUser(this.formUsername, this.formPassword)
      .subscribe((success) => {
        this.formSubmitted = true;
        this.formSubmissionSuccessful = success;
        if (success) {
          this.messageService.sendMessage(
            'Account created successfully! Welcome to Tasks!',
            'alert-success'
          );
        } else {
          this.messageService.sendMessage(
            'Account creation failed.',
            'alert-danger'
          );
        }
      });
  }

  usernameValid(): boolean {
    return this.formUsername.length > 0;
  }

  usernameInvalid(): boolean {
    return this.formUsername.length > 0 && !this.usernameValid();
  }

  passwordValid(): boolean {
    return this.formPassword.length >= 8;
  }

  passwordInvalid(): boolean {
    return this.formPassword.length != 0 && this.formPassword.length < 8;
  }

  passwordVerifyValid(): boolean {
    return (
      this.formPassword == this.formPasswordVerify &&
      this.formPassword.length > 0 &&
      this.formPasswordVerify.length > 0
    );
  }

  passwordVerifyInvalid(): boolean {
    return (
      this.formPassword != this.formPasswordVerify &&
      this.formPassword.length > 0 &&
      this.formPasswordVerify.length > 0
    );
  }
}
