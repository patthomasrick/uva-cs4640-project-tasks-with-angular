import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessagesService } from '../messages.service';
import { EMPTY_USER, User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formUsername: string = '';
  formPassword: string = '';

  constructor(
    private userService: UserService,
    private router: Router,
    private messageService: MessagesService
  ) {
    document.title = 'Tasks | Login';
    this.userService.getUser().subscribe((user) => {
      if (user != EMPTY_USER) {
        this.router.navigate(['']);
      }
    });
  }

  ngOnInit(): void {}

  onSubmit() {
    this.userService
      .login(this.formUsername, this.formPassword)
      .subscribe((success) => {
        if (success) {
          this.router.navigate(['']);
        } else {
          this.messageService.sendMessage(
            'Failed to login. Either the username and password is incorrect or the account does not exist.',
            'alert-warning'
          );
        }
      });
  }
}
