import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessagesService {
  message: Subject<string> = new Subject();
  messageClass: Subject<string> = new Subject();

  constructor() {
    this.message.subscribe((message) => {
      console.log('New message:', message);
    });
  }

  sendMessage(message: string, messageClass: string) {
    this.message.next(message);
    this.messageClass.next(messageClass);
  }

  onMessageClick() {
    this.message.next('');
  }
}
