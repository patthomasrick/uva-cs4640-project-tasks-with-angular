import { Component, HostBinding } from '@angular/core';
import { MessagesService } from '../messages.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({ opacity: '90%' })),
      state('closed', style({ opacity: '0%' })),
      transition('open => closed', [animate('0.5s ease-in')]),
      transition('closed => open', [animate('0.5s ease-out')]),
    ]),
  ],
})
export class MessagesComponent {
  message: string = '';
  messageClass: string = '';
  open: boolean = false;

  constructor(private messagesService: MessagesService) {
    this.messagesService.message.subscribe((v) => {
      this.message = v;
      this.open = true;

      // Close the notification automatically after 5 seconds.
      setTimeout(() => {
        if (this.message == v) {
          this.onMessageClick();
        }
      }, 5000);
    });
    this.messagesService.messageClass.subscribe((v) => {
      this.messageClass = v;
    });
  }

  onMessageClick() {
    this.open = false;
    setTimeout(() => {
      this.message = '';
      this.messageClass = '';
    }, 1000);
  }
}
