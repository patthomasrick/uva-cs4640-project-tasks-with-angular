import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY_USER, User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  user: User = EMPTY_USER;

  constructor(private userService: UserService, private router: Router) {
    this.getUser();
  }

  getUser() {
    this.userService.getUser().subscribe((user) => {
      this.user = user;
    });
  }

  ngOnInit(): void {}

  onLogout() {
    this.userService.logOut();
    this.router.navigate(['login']);
  }
}
