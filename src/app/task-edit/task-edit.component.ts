import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EMPTY_TASK, Task } from '../task';
import { EMPTY_USER, User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss'],
})
export class TaskEditComponent {
  @Input() task: Task = EMPTY_TASK;
  @Output() taskDeleted = new EventEmitter<boolean>();

  user: User = EMPTY_USER;

  constructor(private userService: UserService) {
    userService.getUser().subscribe((user) => {
      this.user = user;
    });
  }

  onDeleteClick() {
    this.userService.deleteTask(this.task).subscribe((success) => {
      this.close();
    });
  }

  close() {
    this.taskDeleted.emit(true);
    this.task = EMPTY_TASK;
  }

  taskModelChange(task: Task) {
    this.userService.updateTask(task).subscribe((success) => {});
  }
}
