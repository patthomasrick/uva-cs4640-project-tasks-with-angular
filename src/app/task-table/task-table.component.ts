import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from '../task';
import { UserService } from '../user.service';

@Component({
  selector: 'app-task-table',
  templateUrl: './task-table.component.html',
  styleUrls: ['./task-table.component.scss'],
})
export class TaskTableComponent {
  @Input() tasks: Task[] = [];
  @Output() clicked = new EventEmitter<Task>();

  constructor(private userService: UserService) {}

  taskModelChange(task: Task) {
    this.userService.updateTask(task).subscribe((success) => {});
  }
}
