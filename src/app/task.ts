export class Task {
  id: number;
  username: string;
  title: string;
  date_created: Date;
  is_deleted: boolean;
  is_done: boolean;
  description?: string;
  date_due?: Date;
  date_scheduled?: Date;
  category?: string;

  constructor(
    id: number,
    username: string,
    title: string,
    date_created: Date,
    is_deleted: boolean,
    is_done: boolean,
    description?: string,
    date_due?: Date,
    date_scheduled?: Date,
    category?: string
  ) {
    this.id = id;
    this.username = username;
    this.title = title;
    this.date_created = date_created;
    this.is_deleted = is_deleted;
    this.is_done = is_done;
    this.description = description;
    this.date_due = date_due;
    this.date_scheduled = date_scheduled;
    this.category = category;
  }

  public isDueToday(): boolean {
    let today = new Date();
    if (!this.date_due) {
      return false;
    }
    return today >= this.date_due;
  }

  public isScheduledToday(): boolean {
    let today = new Date();
    if (!this.date_scheduled) {
      return false;
    }
    return today >= this.date_scheduled;
  }
}

export const EMPTY_TASK: Task = new Task(-1, '', '', new Date(), false, false);
