import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY_TASK, Task } from '../task';
import { EMPTY_USER, User } from '../user';
import { UserService } from '../user.service';

const ONE_DAY: number = 1000 * 60 * 60 * 24;
const CURRENT_DAY_OF_WEEK: number = new Date().getDay();
const NEXT_DAY_TABLE: Map<number, number> = new Map([
  [CURRENT_DAY_OF_WEEK, ONE_DAY * 7],
  [(CURRENT_DAY_OF_WEEK + 1) % 7, ONE_DAY * 1],
  [(CURRENT_DAY_OF_WEEK + 2) % 7, ONE_DAY * 2],
  [(CURRENT_DAY_OF_WEEK + 3) % 7, ONE_DAY * 3],
  [(CURRENT_DAY_OF_WEEK + 4) % 7, ONE_DAY * 4],
  [(CURRENT_DAY_OF_WEEK + 5) % 7, ONE_DAY * 5],
  [(CURRENT_DAY_OF_WEEK + 6) % 7, ONE_DAY * 6],
]);

interface TokensDate {
  tokens: string[];
  date: Date | null;
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent {
  user: User = EMPTY_USER;

  allCategories: string[] = [];
  todayTasks: Task[] = [];
  allTasks: Task[] = [];

  quickAddText: string = '';
  partialTask: Task = EMPTY_TASK;

  editTaskTarget: Task = EMPTY_TASK;
  editTaskDialogVisible: boolean = false;

  constructor(private userService: UserService, private router: Router) {
    this.userService.getUser().subscribe((user) => {
      this.user = user;
      if (this.user.username == EMPTY_USER.username) {
        this.router.navigate(['/login']);
      }

      this.allTasks = this.user.tasks;
      this.allTasks = this.allTasks.sort(this.taskSort);

      // Grab all tasks that are due or scheduled for today.
      this.todayTasks = [];
      for (let index = 0; index < this.user.tasks.length; index++) {
        let t: Task = this.user.tasks[index];
        if (
          (t.isDueToday() || t.isScheduledToday()) &&
          !(t.is_done || t.is_deleted)
        ) {
          this.todayTasks.push(t);
        }
      }

      // Collect a list of categories.
      this.allCategories = [];
      this.allTasks.forEach((task) => {
        if (task.category && this.allCategories.indexOf(task.category) == -1) {
          this.allCategories.push(task.category);
        }
      });
      this.allCategories.sort();
    });
    document.title = 'Tasks | Dashboard';
  }

  quickAddParse() {
    let t: Task = new Task(
      -1,
      this.user.username,
      '',
      new Date(),
      false,
      false
    );

    // Parse out the string.
    let tokens = this.quickAddText.trim().split(' ');

    let tokensDueDate = this.getDateByMarker(tokens, ['on', 'due']);
    let tokensScheduledDate = this.getDateByMarker(tokensDueDate.tokens, [
      'by',
      'sched',
      'scheduled',
    ]);

    t.title = tokensScheduledDate.tokens.join(' ');
    if (tokensDueDate.date) {
      t.date_due = tokensDueDate.date;
    }
    if (tokensScheduledDate.date) {
      t.date_scheduled = tokensScheduledDate.date;
    }

    tokens = tokensScheduledDate.tokens;

    let categoryIndex = tokens.findIndex((s) => {
      return s.length > 0 && s[0] == '#';
    });
    if (categoryIndex >= 0) {
      t.category = tokens[categoryIndex].slice(1, tokens[categoryIndex].length);
      tokens.splice(categoryIndex);
    }

    t.title = tokens.join(' ');

    this.partialTask = t;
  }

  quickAdd() {
    // Partial task should be complete.
    this.userService.addTask(this.partialTask).subscribe((success) => {
      if (success) {
        this.quickAddText = '';
        this.partialTask = EMPTY_TASK;
      }
    });
  }

  private taskSort(a: Task, b: Task): number {
    let a_date = 0;
    if (a.is_done) {
      a_date = 0;
    } else if (a.date_scheduled) {
      a_date = a.date_scheduled.getTime();
    } else if (a.date_due) {
      a_date = a.date_due.getTime();
    } else {
      a_date = a.id;
    }
    let b_date = 0;
    if (b.is_done) {
      b_date = 0;
    } else if (b.date_scheduled) {
      b_date = b.date_scheduled.getTime();
    } else if (b.date_due) {
      b_date = b.date_due.getTime();
    } else {
      b_date = b.id;
    }
    return b_date - a_date;
  }

  private getDateByMarker(tokens: string[], markers: string[]): TokensDate {
    let outputDate: Date | null = null;

    var markerIndex = tokens.findIndex((s) => {
      return markers.includes(s.toLowerCase());
    });

    if (markerIndex == -1) {
      return {
        tokens: tokens,
        date: null,
      };
    }

    if (markerIndex + 1 < tokens.length) {
      let s = tokens[markerIndex + 1];
      if (['today', 'tod', 'now'].includes(s)) {
        outputDate = new Date();
        tokens.splice(markerIndex, 2);
      } else if (['tomorrow', 'tommorow', 'tom'].includes(s)) {
        outputDate = new Date();
        outputDate.setDate(outputDate.getDate() + 1);
        tokens.splice(markerIndex, 2);
      } else if (['yesterday', 'yest'].includes(s)) {
        outputDate = new Date();
        outputDate.setDate(outputDate.getDate() - 1);
        tokens.splice(markerIndex, 2);
      } else if (['sun', 'sunday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(0);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['mon', 'monday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(1);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['tues', 'tuesday', 'tue'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(2);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['wed', 'wednesday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(3);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['thu', 'thurs', 'thur', 'thursday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(4);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['fri', 'friday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(5);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else if (['sat', 'saturday'].includes(s)) {
        let nextDay = NEXT_DAY_TABLE.get(6);
        if (nextDay) {
          outputDate = new Date(new Date().getTime() + nextDay);
          tokens.splice(markerIndex, 2);
        }
      } else {
        // Guess until the date doesn't work.
        let lastValidTokens = 0;
        let lastValidDate = null;
        let i = markerIndex + 2;
        for (i; i <= tokens.length; i++) {
          let subTokens = tokens.slice(markerIndex + 1, i);
          let newDate = new Date(Date.parse(subTokens.join(' ')));
          if (this.isValidDate(newDate)) {
            // Set year if necessary.
            if (newDate.getFullYear() < new Date().getFullYear()) {
              newDate.setFullYear(new Date().getFullYear());
            }
            lastValidDate = newDate;
            lastValidTokens = subTokens.length;
          }
        }
        if (lastValidTokens > 0) {
          tokens.splice(markerIndex, lastValidTokens + 1);
          outputDate = lastValidDate;
        }
      }
    }

    if (outputDate) {
      outputDate.setHours(0);
      outputDate.setMinutes(0);
      outputDate.setSeconds(0);
      outputDate.setMilliseconds(0);
    }

    return {
      tokens: tokens,
      date: outputDate,
    };
  }

  private isValidDate(d: any) {
    // From https://stackoverflow.com/a/1353711
    return !isNaN(d) && d instanceof Date;
  }

  getTasksByCategory(category: string) {
    let output: Task[] = [];
    this.allTasks.forEach((task) => {
      if (task.category === category) {
        output.push(task);
      }
    });
    output = output.sort();
    return output;
  }

  markAsDone(task: Task) {
    task.is_done = true;
  }

  clickedTask(task: Task) {
    this.editTaskTarget = task;
    this.editTaskDialogVisible = true;
    document.title = 'Tasks | Editing ' + this.editTaskTarget.title;
  }

  exitDialog() {
    this.editTaskTarget = EMPTY_TASK;
    this.editTaskDialogVisible = false;
    document.title = 'Tasks | Dashboard';
  }
}
