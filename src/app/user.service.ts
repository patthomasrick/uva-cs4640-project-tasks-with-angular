import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MessagesService } from './messages.service';
import { Task } from './task';
import { EMPTY_USER, User } from './user';

interface IsLoggedInResponse {
  success: boolean;
}

interface LoginResponse {
  success: boolean;
  user: User;
  request?: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  username: string = '';
  password: string = '';
  user: Subject<User> = new ReplaySubject(1);

  constructor(
    private http: HttpClient,
    private messageService: MessagesService,
    private router: Router
  ) {
    this.getUser().subscribe((user) => {
      // console.log('User: ', user);
    });

    let username = window.localStorage.getItem('username');
    let password = window.localStorage.getItem('password');

    if (username && password) {
      this.login(username, password).subscribe((success) => {
        if (!success) {
          this.user.next(EMPTY_USER);
        }
      });
    } else {
      this.user.next(EMPTY_USER);
    }

    // this.login('patrick', 'password');
  }

  login(username: string, password: string): Observable<boolean> {
    this.username = username;
    this.password = password;

    window.localStorage.setItem('username', username);
    window.localStorage.setItem('password', password);

    return new Observable((observer) => {
      this.http
        .post<LoginResponse>(environment.USER_LOGIN_URL, {
          username: username,
          password: password,
        })
        .subscribe((data) => {
          if (data.success) {
            // Clean the returned user's tasks a bit.
            data.user = this.cleanResponseUser(data.user);
            this.user.next(data.user);
          }
          observer.next(data.success);
        });
    });
  }

  private cleanResponseUser(user: User): User {
    for (let i = 0; i < user.tasks.length; i++) {
      const task = user.tasks[i];
      task.date_created = new Date(task.date_created);
      if (task.date_due) {
        task.date_due = new Date(task.date_due);
      }
      if (task.date_scheduled) {
        task.date_scheduled = new Date(task.date_scheduled);
      }
      task.is_deleted = Boolean(Number(task.is_deleted));
      task.is_done = Boolean(Number(task.is_done));

      let c_task = new Task(
        task.id,
        task.username,
        task.title,
        task.date_created,
        task.is_deleted,
        task.is_done,
        task.description,
        task.date_due,
        task.date_scheduled,
        task.category
      );
      user.tasks[i] = c_task;
    }
    return user;
  }

  createUser(username: string, password: string): Observable<boolean> {
    this.username = username;
    this.password = password;
    return new Observable((observer) => {
      this.http
        .post<LoginResponse>(environment.USER_CREATE_URL, {
          username: username,
          password: password,
        })
        .subscribe((data) => {
          if (data.success) {
            this.login(this.username, this.password).subscribe((success) => {
              observer.next(data.success);
            });
          } else {
            observer.next(data.success);
          }
        });
    });
  }

  changeUserPassword(new_password: string): Observable<boolean> {
    return new Observable((observer) => {
      this.http
        .post<LoginResponse>(environment.USER_CHANGE_PASSWORD_URL, {
          username: this.username,
          password: this.password,
          new_password: new_password,
        })
        .subscribe((data) => {
          if (data.success) {
            this.password = new_password;
            this.login(this.username, this.password);
          }
          observer.next(data.success);
        });
    });
  }

  getUser(): Observable<User> {
    return this.user;
  }

  updateUser(): Observable<User> {
    this.login(this.username, this.password);
    return this.user;
  }

  logOut() {
    this.username = '';
    this.password = '';
    window.localStorage.removeItem('username');
    window.localStorage.removeItem('password');
    this.user.next(EMPTY_USER);
  }

  addTask(task: Task) {
    return new Observable((observer) => {
      let postData = {
        username: this.username,
        password: this.password,
        title: task.title,
        category: task.category,
        description: task.description,
        date_due: '',
        date_scheduled: '',
      };
      if (task.date_due) {
        postData['date_due'] = task.date_due.toLocaleString('en-US');
      }
      if (task.date_scheduled) {
        postData['date_scheduled'] = task.date_scheduled.toLocaleString(
          'en-US'
        );
      }
      this.http
        .post<LoginResponse>(environment.TASK_ADD_URL, postData)
        .subscribe((data) => {
          this.user.next(this.cleanResponseUser(data.user));
          observer.next(data.success);
        });
    });
  }

  deleteTask(task: Task) {
    return new Observable((observer) => {
      let postData = {
        username: this.username,
        password: this.password,
        id: task.id,
      };
      this.http
        .post<LoginResponse>(environment.TASK_DELETE_URL, postData)
        .subscribe((data) => {
          this.user.next(this.cleanResponseUser(data.user));
          observer.next(data.success);
        });
    });
  }

  deleteUser() {
    return new Observable((observer) => {
      let postData = {
        username: this.username,
        password: this.password,
      };
      this.http
        .post<LoginResponse>(environment.USER_DELETE_URL, postData)
        .subscribe((data) => {
          if (data.success) {
            this.logOut();
          }
          observer.next(data.success);
        });
    });
  }

  updateTask(task: Task) {
    return new Observable((observer) => {
      let postData = {
        username: this.username,
        password: this.password,
        id: task.id,
        title: task.title,
        description: task.description,
        date_due: '',
        date_scheduled: '',
        category: task.category,
        is_done: task.is_done,
        is_deleted: task.is_deleted,
      };
      if (task.date_due) {
        postData['date_due'] = task.date_due.toLocaleString('en-US');
      }
      if (task.date_scheduled) {
        postData['date_scheduled'] = task.date_scheduled.toLocaleString(
          'en-US'
        );
      }
      this.http
        .post<LoginResponse>(environment.TASK_UPDATE_URL, postData)
        .subscribe((data) => {
          this.user.next(this.cleanResponseUser(data.user));
          observer.next(data.success);
        });
    });
  }
}
