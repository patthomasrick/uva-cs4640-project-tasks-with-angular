import { Task } from './task';

export interface User {
  username: string;
  session_id: string;
  user_meta: Map<String, String>;
  tasks: Task[];
}

export const EMPTY_USER: User = {
  username: '',
  session_id: '',
  user_meta: new Map(),
  tasks: [],
};
