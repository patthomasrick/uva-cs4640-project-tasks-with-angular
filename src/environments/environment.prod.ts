export const environment = {
  production: true,
  USER_LOGIN_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/user/login.php',
  USER_CREATE_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/user/create.php',
  USER_VERIFY_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/user/verify.php',
  USER_DELETE_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/user/delete.php',
  USER_CHANGE_PASSWORD_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/user/change_password.php',
  TASK_ADD_URL: 'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/tasks/add.php',
  TASK_DELETE_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/tasks/delete.php',
  TASK_UPDATE_URL:
    'https://cs4640-web-pl-pwt5ca.ue.r.appspot.com/tasks/update.php',
};
