// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  USER_LOGIN_URL: 'http://localhost:8080/user/login.php',
  USER_CREATE_URL: 'http://localhost:8080/user/create.php',
  USER_VERIFY_URL: 'http://localhost:8080/user/verify.php',
  USER_DELETE_URL: 'http://localhost:8080/user/delete.php',
  USER_CHANGE_PASSWORD_URL: 'http://localhost:8080/user/change_password.php',
  TASK_ADD_URL: 'http://localhost:8080/tasks/add.php',
  TASK_DELETE_URL: 'http://localhost:8080/tasks/delete.php',
  TASK_UPDATE_URL: 'http://localhost:8080/tasks/update.php',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
